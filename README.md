# Pencil documentation website

This is the documentation website for [**Pencil**](https://github.com/blenderskool/pencil) which is a static site generator based on markdown. This site is also built with Pencil
as an example to show how it works with real websites.

## Contribute
To contribute to this site, follow the given commands.
```bash
git clone https://gitlab.com/blenderskool/pencil-site
cd pencil-site
npm install
```
All the content for the site can be found in `src` folder, each Markdown file here is converted to a static HTML page once built.
Site configuration settings can be found in `pencil.config.js` file. The `plugins` directory contains the plugins used in the project and also acts as a CDN service to allow
other projects to use these plugins directly, without downloading them.

### Dev server
```bash
npm run dev
```

### Build for production
```bash
npm run build
```

## License
This project is [MIT Licensed](https://gitlab.com/blenderskool/pencil-site/blob/master/LICENSE)