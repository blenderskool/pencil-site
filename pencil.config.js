module.exports = {
  head: {
    title: 'Pencil | Static documentation website generator',
    link: [{
      href: 'https://unpkg.com/ionicons@4.2.4/dist/css/ionicons.min.css',
      rel: 'stylesheet'
    }]
  },
  navigation: [
    ['Config reference', '/config'],
    ['<i class="icon ion-logo-github"></i> GitHub', {
      link: 'https://github.com/blenderskool/pencil',
      newTab: true
    }]
  ],
  darkTheme: {
    toggle: true
  },
  logo: '/img/logo.svg',
  themeColor: '#287BE1',
  plugins: {
    callout: 'plugins/callout.js',
    youtube: 'plugins/youtube.js',
    icon: 'plugins/icons.js'
  },
  sidebar: [
    ['Introduction', '/'],
    ['Getting Started', {
      Installation: '/getting-started',
      'Directory structure': '/getting-started/directory.html',
      'Development server': '/getting-started/development.html',
      'First page': '/getting-started/first-page.html'
    }],
    ['Plugins', '#'],
    ['File paths', '#'],
    ['Deploy', '/deploy'],
  ],
  footer: [
    ['MIT Licensed']
  ]
}