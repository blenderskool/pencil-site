---
title: Config Reference | Pencil
---

# Configuration

All the options are in the `pencil.config.js` file. This file returns a JavaScript object with all the options that Pencil must use to build your site.


## head
- Type: `Object`
- Default: `{}`

Creates the `<head>` section for your site. Tags can be defined as an Array of Objects like `tag-name: [{attribute-name: value ...}, ...]`. For example, to include meta, script tags:

```javascript
module.exports: {
  head: {
    meta: [
      { charset: 'UTF-8' },
      { name: 'description', content: 'Hello Pencil' }
    ],
    script: [
      { src: '/scripts.js' }
    ],
    title: 'Pencil site'
  }
}
```

<callout type="warning">
  Title must be added using a [different syntax](#title).
</callout>

<callout type="error">
  `theme-color` meta tag is not supported. [Use `themeColor` option](#themecolor)
</callout>

### title
- Type: `String`
- Default: `'Pencil site'`

Defines a **global title** of your entire site. Pages not using a specific title would fallback to this title.

<callout>
  Title for a specific page can be specified using [front matter](#).
</callout>



## navigation
- Type: `Array`
- Default: `[]`

Creates the navigation bar on the site. General syntax is
```javascript
[ 'linkText', options ]
```

<callout>
  Objects are **not** used as this syntax allows you to use multiple links with same name, but different URLs.
</callout>

The array can take a mix of the following values:

### Links
Links can be created by passing a `String` value in the `options`. Therefore, `['linkText', 'linkURL']` syntax is used for creating links. 

To open a link in new tab, the `newTab` option must be passed in an object instead of `'linkURL'`. So, the syntax becomes:
```javascript
['linkText', {
  link: 'linkURL',
  newTab: true
}]
```

### Text
Static text can be created by just adding a `String` value. This can be useful when you want to display some text throughout the site.

### Drop-down menus
Drop-down menus are supported upto a **single level depth**. This can be created using a syntax similar to Links. An array is passed in the `options`. For example:
```javascript
['Foo', [
  [ 'Bar', '#' ],
  [ 'Baz', {
    link: '#baz',
    newTab: true
  }]
]]
```
Within the array, you can include additional links, text following the **same syntax mentioned above**.

The following example illustrates how to create a navigation bar with links, text, drop-down menus.

```javascript
module.exports = {
  navigation: [
    ['Foo', '#'], // Standard link
    ['Google', {  // Open in new tab link
      newTab: true,
      link: 'https://google.com'
    }],
    'Hello World', // Text,
    ['Menu', [  // Drop down menu
      ['1', '#'] 
    ]]
  ]
}
```

<callout>
  **Drop-down** menus are supported in Header navigation.<br/>
  **Drop-up** menus are supported in Footer.
</callout>


## logo
- Type: `String`
- Default: `undefined`

Adds a logo to the header of all the web pages. A valid path to the file must be given.

<callout>
  It is recommended to **include all images** other than markdown files in the `static` folder. [Read more](/getting-started/directory.html#static)
</callout>

<callout type="error">
  Files within `static` folder are in the `root` directory of the site. Hence the file path must be given, assuming that the file exists in the root. [Read more on file paths](#)
</callout>

## themeColor
- Type: `String`
- Default: `'#287BE1'`

Sets the main theme color of the website. This color is **also used to set the `theme-color` meta tag**. Value of this option must be in a valid CSS color format. 

This color **can be used** within the [plugin styles](#), and is used deeply within the core CSS styles.

## darkTheme
- Type: `Boolean | Object`
- Default: `false`

Enables the dark theme for the site. The `Boolean` type is used if you want to set the **dark theme by default** for all the pages in the site.

Additionally, an `Object` can be passed with other options given below:

### default
- Type: `Boolean`
- Default: `false`

Sets the dark theme by default for the entire site. It is the same as directly setting `darkTheme: true`.

### toggle
- Type: `Boolean`
- Default: `false`

If enabled, a **toggle button would be shown** in the header, that would allow the user to **switch between light/dark** themes as per their convenience. This also makes use of **session storage**, to store the user theme choice for the particular session.

## plugins
- Type: `Object`
- Default: `{}`

This object is used to **load the plugins** that would be used in Pencil while development. The syntax used is `{ tagName: 'plugin-path' }`. `tagName` defines the **custom tag** that would be used in markdown to use the specified plugin.

Following plugins configuration comes with the starter template of Pencil:

```javascript
plugins: {
  callout: 'plugins/callout.js',
  youtube: 'plugins/youtube.js',
}
```

<callout type="success">
  Storing the plugin files in `plugins` folder is not mandatory.
</callout>

<callout>
  Use a `tagName` that is different from
  <a href="https://www.tutorialrepublic.com/html-reference/html5-tags.php" target="_blank">
    HTML5 tags
  </a> for the plugins to avoid conflicts and errors.
</callout>

## sidebar
- Type: `Array`
- Default: `[]`

Creates the sidebar navigation. It follows the same syntax as the navigation object.
[Read more on navigation syntax](#navigation).  
Additionally, **sidebar also supports plugins** :tada:. For example, to use the callout plugin:

```javascript
sidebar: [
  ['callout', {
    type: 'success',
    children: 'Hello World'
  }]
]
```

So the plugin tag name is used as the first element in array, followed by an object which includes the **attribute/value pairs**. `children` key is used to set innerText of the plugin element.

## styles
- Type: `Array`
- Default: `[]`

Array of `strings` of path to additional CSS files to be included in the build process. All the styles are bundled, minified together.

## scripts
- Type: `Array`
- Default: `[]`

Array of `strings` of path to additional JavaScript files to be included in the build process. All the scripts are bundled, minified together.

## footer
- Type: `Array`
- Default: `[]`

Creates the footer of the site. It follows the same syntax as the navigation object. [Read more on navigation syntax](#navigation)