---
title: Deployment | Pencil
---

# Deployment

Once you have your content ready, Pencil can do the heavy lifting on making it into a static website.

## Build the site
Your site can be built by running the following command in the **root directory** of your project.

```bash
npm run build
```

The site generated will be in the `dist` folder, and can be uploaded to a webhost.


## Using Netlify
<a href="https://www.netlify.com" target="_blank">Netlify</a> allows you to easily connect your deployments to your Git repository. This means, that when you push the commits to the origin, Netlify will automatically take the source code, and build the site on its servers.

Pencil can be easily used for a Netlify site.