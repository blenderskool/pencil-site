---
title: Development server | Pencil
---

# Development server

Pencil comes built-in with a dev server which runs on <a href="https://browsersync.io" target="_blank">BrowserSync</a>. It supports **live reload**, and creates a **LAN web server** which can be accessed by devices connected to the same network.

## Start dev server
You can start the development server by running the following command in the root directory of the project:

```bash
npm run dev
```

This will start a server on port `2000` which is set by default. This can be changed using the `--port` option that comes with this command.

You can access the **BrowserSync UI** on the default port `3001` or the port specified in the terminal.


## Options
The `dev` command comes with the following options that can be used to change the settings of the development server.

### --port, -p
This option changes the default port to the specified port. For example, to change the port to 3000,

```bash
npm run dev -- --port 3000
```

In the `package.json` file, under the **scripts** object, you can change the `dev` command to `pencil dev --port 3000` to use this port always automatically.