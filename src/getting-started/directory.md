---
title: Directory structure | Pencil
---

# Directory structure

Pencil site template comes with a pre-defined directory structure, that should be used while creating your site. You should probably have a  structure that looks like this:

```text
.
├─ src
│  └─ index.md
├─ plugins
│  ├─ callout.js
│  └─ youtube.js
└─ pencil.config.js
```

Certain folders are **neccessary** for the functioning of Pencil. Additionally, some folders are optional, and should be used only if you are using that specific feature of Pencil. They can be found below.

## Required folders

### src
This folder contains **all the markdown files** that will be converted to HTML pages. Additional subfolders can be added here for better grouping of content.

<callout warning="true">
Never add images, or other media files to the `src` folder. They should be added to the [static](#static) folder instead.
</callout>

## Optional folders
These folders are optional, and must be added, only if those features are being used.

### static
This folder contains all the **static content** that will be used in your app. It can **include images, other files** that should not be present in the `src` folder. These files will be copied directly to the output folder, where it would be used with rest of the HTML files.

### plugins
This folder contains the **JavaScript plugin files** that would be used in the site. They should be **manually** registered in the config file.


## Config file
> **Required** file

The config file is called `pencil.config.js` which is a file that exports a JavaScript object that contains all the **options** for Pencil:

```javascript
module.exports = {
  head: {
    title: 'Pencil site'
  },
  themeColor: '#287BE1'
}
```

Options can be modified as and when required in your site.

<callout warning="true">
The config file should **not be relocated**, and is always present at the **root** of your project folder.
</callout>

[Check config reference](/config) to see the full list of options that can added to Pencil
