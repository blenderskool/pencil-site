---
title: First page | Pencil
---

# Creating your first page

In the `src` folder start by creating a file called `index.md`. This file will be the root HTML file that will be created as `index.html`. You can start writing your content directly in this file.

Given below are a **few** tips that you can follow to get the most of the styling of Pencil

## Tips for content styling

### Headings
Headings must be used correctly, as they divide your page in different sections, and also make the site more accessible to people using screen readers.

- `# Heading 1` - Used **once** as the main title of the page.
- `## Heading 2` - Divides the content in **different sections**.
- `### Heading 3` - **Subheadings** in each section.

### Callouts
Callouts can be used to point some **important information** that the reader should know. There are 4 types of callouts that come with the plugin.

```html
<callout type="(TYPE)">
  This is a callout
</callout>
<!-- (TYPE) can be either info(default), success,
warning, error -->
```
<callout type="success">
  This is a success callout
</callout>

### YouTube video
YouTube video can be embedded using the **YouTube video ID** found in the URL.

```html
<youtube>
(VideoID)
</youtube>
```