---
title: Installation | Pencil
---

# Getting Started

Getting started with Pencil is really easy. Just follow the steps given below.

## Requirements

- Make sure you have recent LTS version of Node.js installed. [Download Node.js](https://nodejs.org/en/)
- A basic understanding of Node.js helps, but not necessary.

## Quick start

Pencil site can be created quickly using the CLI tool.

```bash
npm install -g @penciljs/core
pencil init
```
There is a `package.json` file which is created in the project root directory.

<callout>
	Installing dependencies by `npm install` is **not** necessary as Pencil is already globally installed.
</callout>

## Manual way

The following commands can be used to start with the Pencil site template.
```bash
git clone https://github.com/blenderskool/pencil-template pencil-site
cd pencil-site
git remote rm origin
npm install
```