<p align="center">
<!-- ![Pencil](/img/logo_icon.svg "Pencil") -->
  <img src="/img/logo_icon.svg" alt="Pencil">
</p>

<p align="center">
   <a href="https://www.npmjs.com/package/@penciljs/core">
    <img src="https://img.shields.io/npm/v/@penciljs/core.svg" alt="Version">
  </a>
  <a href="https://www.npmjs.com/package/@penciljs/core">
    <img src="https://img.shields.io/npm/l/@penciljs/core.svg" alt="License">
  </a>
</p>

# Pencil

Pencil is a powerful **static documentation website generator** that works on **Markdown**. Main features of Pencil include:
- Fast site generation
- Great SEO, as static files are created
- Plugin support
- Easy customization

Start by following the [quick start guide](/getting-started#quick-start)

## What can it do?

Pencil allows you to write content in [Markdown](https://en.wikipedia.org/wiki/Markdown), which has a very **powerful** and **easy** to learn markup syntax. You can quickly start creating your website without much code.

Pencil will automatically create the required files for your website, which are **ready to be deployed** to services like Netlify, GitHub Pages.

## Main features
Pencil was built with a goal to provide the most out of the box. It comes with useful plugins that would enhance your content to the next level.

### SEO
Pencil generates completely static content, which means they can be easily hosted on shared hosting, without you having to invest in a powerful server.

### Plugins
Plugins help you extend the functionality of Pencil. Features such as YouTube player embed, Callouts, etc. are made possible using the plugins.  
They can also be used like simple components, which can be easily created and reused in different parts of your markdown file.

### Easy customization
Customize the colors, styles of your site. Just include your CSS files in the Pencil config file, and it will be given more priority over the default styles.